package com.classpath.orders.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
	
	@Bean
	public User user() {
		return new User();
	}
	
	@ConditionalOnProperty(
			prefix = "app", 
			value = "loadUser", 
			havingValue = "true", 
			matchIfMissing = true)
	@Bean
	public User userBasedOnProperty() {
		return new User();
	}
	
	@ConditionalOnBean(name = "userBasedOnProperty")
	@Bean
	public User userBasedOnBean() {
		return new User();
	}
	
	@ConditionalOnMissingBean(name = "userBasedOnProperty")
	@ConditionalOnMissingClass(value = "com.classpath.orders.util.ApplicationUtil")
	@Bean
	public User userBasedOnMissingBean() {
		return new User();
	}
	
	@ConditionalOnMissingClass(value = "com.classpath.orders.util.ApplicationUtil")
	@Bean
	public User userBasedOnMissingClass() {
		return new User();
	}

}

class User {
	
}
